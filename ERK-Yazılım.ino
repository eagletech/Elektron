#include <IBusBM.h>
#include <Servo.h>
 
#define MEGA
#undef MEGA

IBusBM receiver;

/* Channel  1: LX */
/* Channel  2: RY */
/* Channel  3: LY */
/* Channel  4: LX  */
/**/
/* Channel  5: VRA */
/* Channel  6: VRB */
/**/
/* Channel 7:  SWA  */
/* Channel 8:  SWB  */
/* Channel 9:  SWC  */
/* Channel 10: SWD  */


/* PWM Outputs: D3, D5, D6, D9, D10, D11 */
#define BASE_SERVO 2
#define SHOULDER_SERVO_RIGHT 3
#define SHOULDER_SERVO_LEFT 3
#define ELBOW_SERVO 9
#define WRIST_SERVO 10
#define CLAMP_SERVO 11

#define BUZZER 11
#define LEDS 12

#define LOOP_DELAY 10

#define GRAB_SWITCH_CH 6 // real channel number (7) - 1
#define GRAB_SWITCH_CH_L 7 // real channel number (8) - 1

Servo base_servo;
Servo shoulder_servo_right;
Servo shoulder_servo_left;
Servo elbow_servo;
Servo wrist_servo;
Servo clamp_servo;
 
int buzzer_timeout = 0;
bool old_grab_sw_state = 0;

int readChannel(byte channelInput, int minLimit, int maxLimit, int defaultValue);
bool readSwitch(byte channelInput, bool defaultValue);
void buzzer(void);
void grab(bool unl);
void ungrab(void);
void print_rc(void);

void setup() {
#ifdef MEGA
  Serial.begin(9600);
#endif 
 
  base_servo.attach(BASE_SERVO);
  shoulder_servo_right.attach(SHOULDER_SERVO_RIGHT);
  shoulder_servo_left.attach(SHOULDER_SERVO_LEFT);
  elbow_servo.attach(ELBOW_SERVO);
  wrist_servo.attach(WRIST_SERVO);
  clamp_servo.attach(CLAMP_SERVO);

  pinMode(LEDS, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  digitalWrite(BUZZER, LOW);
  
#ifdef MEGA
  receiver.begin(Serial1);
#else
  receiver.begin(Serial);
#endif
}

void loop() {
  
 
  /* BASE_SERVO PWM range: 0 - 60 */ 
  /* GRIPPER_SERVO PWM range: 0 - 180 */ 

  base_servo.write(readChannel(3,0,180,0));
  /* base_servo.write(readChannel(3,0,60,0)); */
  /* shoulder_servo_left.write(readChannel(2,0,180,0)); */
  /* shoulder_servo_right.write(-readChannel(2,0,180,0)); */
  /* elbow_servo.write(readChannel(1,0,180,0)); */
  /* clamp_servo.write(readChannel(0,0,180,0)); */

  /* if (readSwitch(7, true) == old_grab_sw_state); */

  if (buzzer_timeout-- <= 0)
    ungrab();

  if (readSwitch(GRAB_SWITCH_CH, false) != old_grab_sw_state)
    grab(1);
  else if (readSwitch(GRAB_SWITCH_CH_L, false) == 1) {
    grab(0);
  }

#ifdef MEGA
  print_rc();
  Serial.println();
#endif
 
  old_grab_sw_state = readSwitch(GRAB_SWITCH_CH, false);
  delay(LOOP_DELAY);
}

int readChannel(byte channelInput, int minLimit, int maxLimit, int defaultValue) {
  uint16_t ch = receiver.readChannel(channelInput);
  if (ch < 100) return defaultValue;
  return map(ch, 1000, 2000, minLimit, maxLimit);
}
 
bool readSwitch(byte channelInput, bool defaultValue) {
  int intDefaultValue = (defaultValue) ? 100 : 0;
  int ch = readChannel(channelInput, 0, 100, intDefaultValue);
  return (ch > 50);
}

void buzzer(void) {
  int f2 = 1396;
  tone(BUZZER, f2);
}

void grab(bool unl) {
  digitalWrite(LEDS, HIGH);
  buzzer();
  if(unl){
    buzzer_timeout = 1000 / LOOP_DELAY; 
  } else {
  buzzer_timeout = 0;
  }
}

void ungrab(void) {
  digitalWrite(LEDS, LOW);
  noTone(BUZZER);
  buzzer_timeout = 0;
}

void print_rc(void) {
#ifdef MEGA
  for (byte i = 0; i < 6; i++) {
    int value = readChannel(i, 0, 360, 0);
    Serial.print(value);
    Serial.print(" ");
#else
  return;
#endif
}
